---
layout: handbook-page-toc
title: How to Get Help
category: Handling tickets
description: Workflow for Support Engineers on how to get help when working on a ticket. 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Getting Help on a Ticket

When [working on tickets](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html), collaboration is critical, especially when troubleshooting complex issues, or technical areas of focus that fall outside of your experience level. Asking for help means having a [low level of shame](https://about.gitlab.com/handbook/values/#low-level-of-shame), and also shows that you are putting the customer first because you are working towards resolving their problem.

### How to Get Help Workflow

If you are stuck on a ticket, the following workflow seeks to help Support Engineers realize and utilize all of the resources available to progress a ticket to resolution. This workflow lists some common resources, you can lean on to get the help you need. 

**If you’re stuck on a ticket…..**

Identify what's causing you to get stuck. Some examples are:

* I don't have the right knowledge to progress this ticket.
* The customer's query is out of scope, but they expect us to resolve this.
* There is a deep technical issue which needs a development expert's consult.

Then choose from below the appropriate resource that can help unblock you. [Escalating to unblock](https://about.gitlab.com/handbook/values/#escalate-to-unblock) is a sub-value of Results.

* Follow [the workflow](https://about.gitlab.com/handbook/support/workflows/working-on-tickets.html#what-is-the-the-needs-collaboration-view-and-workflow) to add your ticket to the `Needs Collaboration` view in Zendesk. Use the `Collaboration` macro to summarize the help that you need into an internal note so that others can quickly understand what help is needed. This will add the `needs_collaboration` tag to the ticket, which will add it to the `Needs Collaboration` view.
* Depending on the area of focus (such as self-managed or SaaS) and the urgency, ask for help in the [relevant Support Slack channels](https://about.gitlab.com/handbook/support/#slack).
    - Always link the ticket you need help with.
    - Be specific about the help you need.
        - For example, "Kubernetes Runner help needed: user is running in X error, logs are saying Y, and we've tried Z. What else could it be?"
    - Check the [Skills by Subject](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) Support page to see who might be able to assist based on the knowledge area of the problem. 
    - Mention that person in the thread and the ticket for continued collaboration and guidance.
    - Offer to schedule a pairing session, or work in collaboration with others asynchronously in the Slack thread.
* Ask for help in the appropriate [DevOps Staging](https://about.gitlab.com/handbook/product/categories/#devops-stages) Slack channel, or reach out to the [Support Stable Counterpart](https://about.gitlab.com/handbook/support/support-stable-counterparts.html) for the area of focus you need help in. If you are unsure about this, ask the team and/or a manager for guidance.
    - For example, if you need help with a runners issue, ask for development support in the runners Slack channel, or reach out to the Support stable counterpart(s) that are listed under the [runner group](https://about.gitlab.com/handbook/product/categories/#runner-group) section.
* Attend crush or help sessions such as those noted below (see the [GitLab Support calendar](https://calendar.google.com/calendar/u/0?cid=Z2l0bGFiLmNvbV85YnMxNTllaHJjNXRxZ2x1cjg4ZGpiZDUxa0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t) for times):
    - AMER Senior SE Help Sessions
    - APAC/AMER or EMEA/AMER crush sessions
    - APAC or EMEA crush / collaboration sessions
    - Senior Support Office Hours (varying times)
* If in doubt, ping your manager (or the manager on-call) for guidance. Your manager (or any manager) can help determine next steps. Note that in most cases, either the present team is unable to help (beyond expertise), or at capacity. Please do not let this deter you from asking for help any time you need it.

### Quick Links and Resources
- [Needs Collaboration view](https://gitlab.zendesk.com/agent/filters/360080204660) in ZenDesk.
- Create a Support [pairing session issue](https://gitlab.com/gitlab-com/support/support-pairing).
- [Support Workflows](https://about.gitlab.com/handbook/support/workflows/) to follow relevant troubleshooting workflow.
- [Support Documentation links](https://about.gitlab.com/handbook/support/#documentation) for quick references to helpful GitLab documentation.
- [Skills by Subject](https://gitlab-com.gitlab.io/support/team/skills-by-subject.html) to find a Support Engineer scoped to the skill set needed for help.
- [DevOps Stages](https://about.gitlab.com/handbook/product/categories/#devops-stages) to find the right development or product team to reach out to.
- [Emergency runbooks](https://gitlab.com/gitlab-com/support/emergency-runbook/-/tree/master/.gitlab/issue_templates) with troubleshooting tips, even if not an emergency.
- See which [manager is on-call](https://gitlab.pagerduty.com/escalation_policies#PGNLUZ1) if guidance is needed on something urgent.
